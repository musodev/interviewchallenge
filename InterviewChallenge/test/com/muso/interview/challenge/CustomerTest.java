package com.muso.interview.challenge;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CustomerTest {

	private Customer customer; 
	
	@Before
	public void setUp() {
		Movie regularMovie = new Movie("Shaun of the Dead", Movie.REGULAR);
		Movie newReleaseMovie = new Movie("Logan", Movie.NEW_RELEASE);
		Movie childrensMovie = new Movie("Finding Nemo", Movie.CHILDRENS);
		
		Rental regularRental = new Rental(7, regularMovie, new Date());
		Rental newReleaseRental = new Rental(3, newReleaseMovie, new Date());
		Rental childrensRental = new Rental(5, childrensMovie, new Date());
		
		customer = new Customer("Bill Gates");
		customer.addRental(regularRental);
		customer.addRental(newReleaseRental);
		customer.addRental(childrensRental);
		
	}
	
	@Test
	public void testStatement() {
		String statement = customer.weeklyStatement();
		
		String expectedStatement = new StringBuilder("Rental Record for Bill Gates\n")
				.append("\tShaun of the Dead\t12\n")
				.append("\tLogan\t9\n")
				.append("\tFinding Nemo\t7\n")
				.append("Amount owed is 28\n")
				.toString();
		Assert.assertEquals("incorrect statement calculation", expectedStatement, statement);
	}
}
