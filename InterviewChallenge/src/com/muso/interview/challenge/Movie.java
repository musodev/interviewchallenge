package com.muso.interview.challenge;

/**
 * Class holding details about movie releases and their price
 * @author adrian
 *
 */
public class Movie extends DomainObject {

	public static final int  CHILDRENS = 2;
    public static final int  REGULAR = 0;
    public static final int  NEW_RELEASE = 1;

    // release type
	private int priceCode;

	public Movie(String name, int priceCode) {
		setName(name);
		setPriceCode(priceCode);
	}
	
	public int getPriceCode() {
		return priceCode;
	}
	
	public void setPriceCode(int priceCode) {
		this.priceCode = priceCode;
	}
}
