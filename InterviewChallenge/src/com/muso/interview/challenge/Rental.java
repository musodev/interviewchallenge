package com.muso.interview.challenge;

import java.util.Date;

/**
 * Class holding data about customer rentals.
 * @author adrian
 *
 */
public class Rental {

	// number of days this movie is rented for
	private int daysRented;
	
	private Date rentalStartDate;
	
	// rented movie
	private Movie movie;
	
	public Rental(int daysRented, Movie movie, Date rentalStartDate) {
		this.daysRented = daysRented;
		this.movie = movie;
		this.rentalStartDate = rentalStartDate;
	}
	
	public int getDaysRented() {
    	return daysRented;
    }
	public void setDaysRented(int daysRented) {
		this.daysRented = daysRented;
	}
	public Date getRentalStartDate() {
		return rentalStartDate;
	}
	public void setRentalStartDate(Date rentalStartDate) {
		this.rentalStartDate = rentalStartDate;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
}
