package com.muso.interview.challenge;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Customer extends DomainObject {
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Customer.class);

	// rentals for this customer
	private List<Rental> rentals = new ArrayList<Rental>();
	
	// total price of current rentals
	int totalAmount = 0;
	
	public Customer(String name) {
		setName(name);
	}
	
	
	public String weeklyStatement() {
		int sevenDays = 7 * 24 * 60 * 60 * 1000;
		Date startDate = new Date(System.currentTimeMillis() - sevenDays);
		return statement(startDate, rentals, 0);
	}
	
    private String statement(Date statementStartDate, List<Rental> rentals, int discount) {
    	logger.info("Generating new statement for " + getName());
        
        String result = "Rental Record for " + getName() + "\n";
        
        for (Rental each : rentals) {
        	if (each.getRentalStartDate().after(statementStartDate)) {
        	
	        	int thisAmount = 0;
	        	
	        	//determine amounts for each line
	            switch (each.getMovie().getPriceCode()) {
	                case Movie.REGULAR:
	                    thisAmount += 2;
	                    if (each.getDaysRented() > 2)
	                        thisAmount += (each.getDaysRented() - 2) * 2;
	                    break;
	                case Movie.NEW_RELEASE:
	                    thisAmount += each.getDaysRented() * 3;
	                    break;
	                case Movie.CHILDRENS:
	                    thisAmount += 3;
	                    if (each.getDaysRented() > 3)
	                        thisAmount += (each.getDaysRented() - 3) * 2;
	                    break;
	
	            }
	        	
	        	totalAmount += thisAmount;
	        	
	            //show figures for this rental
	            result += "\t" + each.getMovie().getName()+ "\t" + thisAmount + "\n";
        	} else {
        		// do nothing
        	}
        }
        //add footer lines
        result +=  "Amount owed is " + String.valueOf(totalAmount) + "\n";
        return result;

    }
    
    public void addRental(Rental rental) {
    	rentals.add(rental);
    }
    
}
