package com.muso.interview.challenge;

public class DomainObject {
	
	private String name;

	public DomainObject ()	{};

	public String getName ()	{
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}

}
